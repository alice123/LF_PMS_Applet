// custom-tab-bar/index.js
const app = getApp();
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        navIndex: {
            type: Number,
            value: 0
        },
        navRole: {
            type: Number,
            value: 0,
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        // selected: 0,
        selectedColor: '#007dc0',
        list: [{
            pagePath: '/pages/index/index',
            iconPath: '/img/icon/index_off.png',
            selectedIconPath: "/img/icon/index_on.png",
            text: '首 页'
        }, {
            pagePath: '/pages/mine/mine',
            iconPath: '/img/icon/my_off.png',
            selectedIconPath: '/img/icon/my_on.png',
            text: '我 的'
        }]
    },
    ready: function () {
        var that = this;
        console.log(this.data.navRole,this.properties.navRole,'navRole ready');
        console.log('ready tarbar');
        let _this = this;
        if (_this.data.navRole == 1) {
            _this.data.list.splice(1, 0, {
                pagePath: '/pages/record/index',
                iconPath: '/img/icon/record_off.png',
                selectedIconPath: '/img/icon/record_on.png',
                text: '数 据'
            });
        }else{
            if(_this.data.list.length >=3){
                _this.data.list.splice(1,1);
            }
        }
        _this.setData({
            list: _this.data.list
        })
    },
    /**
     * 组件的方法列表
     */
    methods: {
        switchTab(e) {
            const data = e.currentTarget.dataset
            const url = data.path
            wx.switchTab({
                url
            })
        },
    },
})