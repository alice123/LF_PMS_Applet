const util = require('../utils/util.js');
const config = require('../config.js');
const api = {
    // ------------------------------ 登录 ----------------------------------
    /**
     * 解密微信用户手机号码
     * 地址：weixin/mini/mobile
     * 类型：POST
     * 状态码：200
     */
    getPhone: function (param, succ) {
        wx.login({
            success: res => {
                param.code = res.code;
                util.loginPost(config.HTTP_REQUEST_URL + 'weixin/mini/mobile', 'GET', param, succ);
            }
        })
    },
    /**
     * 确认是否已授权登录
     * 地址：auth/weixin/mini
     * 类型：POST
     * 状态码：200
     */
    authorize: function (data) {
        return new Promise(function (resolve) {
            wx.request({
                url: config.HTTP_REQUEST_URL + 'auth/weixin/mini',
                header: {
                    'Authorization': 'Basic aml6aGktcG1zLW1hcmtldC1taW5pLWFwaTpqaXpoaS1wbXMtbWFya2V0LW1pbmktYXBp',
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    resolve(res.data);
                }
            })
        })
    },

    /**
     * 微信绑定
     * 地址：register/weixin/mini
     * 类型：POST
     * 状态码：200
     */
    register: function (param,succ) {
        wx.login({
            success: res => {
                var phoneNumber = wx.getStorageSync('phoneNumber');
                param.code = res.code;
                param.mobile = phoneNumber;
                util.loginPost(config.HTTP_REQUEST_URL + 'register/weixin/mini', 'POST', param, succ);
            }
        })
    },
    /**
     * 获取用户信息
     * 地址：login.user.detail
     * 类型：GET
     * 状态码：200
     */
    userDetail: function (succ) {
        util.request('login.user.detail', 'GET', 'default', {}, succ)
    },
    /**
     * 用户授权订阅消息
     * 地址：msg.weixinTemplate.list
     * 类型：GET
     * 状态码：200
     */
    weixinTemplate: function (param, succ) {
        util.request('msg.weixinTemplate.list', 'GET', 'default', param, succ)
    },
    /**
     * 保存用户授权订阅的模板记录
     * 地址：msg.weixinTemplateSubscribe.create
     * 类型：POST
     * 状态码：200
     */
    weixinTemplateSubscribe: function (param, succ) {
        util.request('msg.weixinTemplateSubscribe.create', 'POST', 'default', param, succ)
    },

    //----------------------------------- 多经管理 ----------------------------------
    /**
     * 获取【点位类型】
     * 地址：mini.res.getDisplayType
     * 类型：GET
     * 状态码：200
     */
    getDisplayType: function (succ) {
        util.request('mini.res.getDisplayType', 'GET', 'default', {}, succ)
    },
    /**
     * 获取【点位级别】
     * 地址：mini.res.getDisplayRank
     * 类型：GET
     * 状态码：200
     */
    getDisplayRank: function (succ) {
        util.request('mini.res.getDisplayRank', 'GET', 'default', {}, succ)
    },
    /**
     * 获取多经列表
     * 地址：mini.res.miniDisplayList
     * 类型：GET
     * 状态码：200
     */
    getDisplayList: function (size, current, param, succ) {
        util.request('mini.res.miniDisplayList?size=' + size + '&current=' + current, 'GET', 'default', param, succ)
    },

    /**
     * 获取多经使用率
     * @param {*} displayType 
     * @param {*} displaySubType 
     * @param {*} succ 
     */
    getDisplayUserRate: function (displayType, displaySubType, succ) {
        util.request('res.display.manager.userRate?displayType=' + displayType + '&displaySubType=' + displaySubType, 'GEt', 'defualt', {}, succ);
    },

    /**
     * 获取经营年预算达成率
     * @param {*} year 
     * @param {*} displayType 
     * @param {*} displaySubType 
     * @param {*} succ 
     */
    getDisplayFinishRate: function (year, type, sutType, succ) {
        util.request('res.display.manager.yearFinishRate?year=' + year + '&displayType=' + type + '&displaySubType=' + sutType, 'GEt', 'defualt', {}, succ);
    },

    /**
     * 经营演示数据接口
     */
    getPhpMini(succ) {
        wx.request({
            url: 'https://shop2.jypet.net/api/v1/mini/index',
            method: 'get',
            data: {},
            success: (res) => {
                succ(res.data);
            },
            fail: (msg) => {
                reject('请求失败');
            }
        })
    },

    // --------------------------------广告管理--------------------------------------------

    /**
     * 获取广告使用率
     * @param {*} yearMonth 
     * @param {*} succ 
     */
    getAdUseRate: function (yearMonth, succ) {
        util.request('res.display.ad.monthUseRate?yearMonth=' + yearMonth, 'GET', 'default', {}, succ);
    },

    /**
     * 获取广告年预算率
     * @param {*} yearMonth 
     * @param {*} succ 
     */
    getAdFinishRate: function (yearMonth, succ) {
        util.request('res.display.ad.monthAccumulateFinishRate?year=' + yearMonth, 'GET', 'default', {}, succ);
    },



    //----------------------------------- 我的审批 ----------------------------------
    /**
     * 获取【我发起】列表
     * 地址：mini/my_process?current=1&size=10
     * 类型：GET
     * 状态码：200
     */
    getMyProcess: function (pageSize, current, succ) {
        util.request('mini/my_process?size=' + pageSize + '&current=' + current, 'GET', 'default', {}, succ)
    },
    /**
     * 获取【未审批】列表
     * 地址：mini/processing?current=1&size=10
     * 类型：GET
     * 状态码：200
     */
    getProcessing: function (pageSize, current, succ) {
        util.request('mini/processing?size=' + pageSize + '&current=' + current, 'GET', 'default', {}, succ)
    },
    /**
     * 获取【已通过】列表
     * 地址：mini/process_pass?current=1&size=10
     * 类型：GET
     * 状态码：200
     */
    getProcessPass: function (pageSize, current, succ) {
        util.request('mini/process_pass?size=' + pageSize + '&current=' + current, 'GET', 'default', {}, succ)
    },
    /**
     * 获取【已驳回】列表
     * 地址：mini/process_reject?current=1&size=10
     * 类型：GET
     * 状态码：200
     */
    getProcessReject: function (pageSize, current, succ) {
        util.request('mini/process_reject?size=' + pageSize + '&current=' + current, 'GET', 'default', {}, succ)
    },
    /**
     * 获取 审批详情
     * 地址：mini/process_form/{formId}
     * 类型：GET
     * 状态码：200
     */
    getProcessDetail: function (formId, succ) {
        util.request('mini/process_form/' + formId, 'GET', 'default', {}, succ)
    },
    /**
     * 提交审批操作
     * 地址：mini/process_complete
     * 类型：PUT
     * 状态码：200
     */
    putProcess: function (param, succ) {
        util.request('mini/process_complete', 'PUT', 'default', param, succ)
    },
    /**
     * 获取 项目审批详情
     * 地址：assignmentRunProject/detail?projectId=
     * 类型：GET
     * 状态码：200
     */
    getProcessingProject: function (formId, succ) {
        util.request('assignmentRunProject/detail?projectId=' + formId, 'GET', 'default', {}, succ)
    },
    //----------------------------------- 我的任务 ----------------------------------
    /**
     * 获取【今日任务】、【逾期任务】和【预期任务】数
     * 地址：assignmentRunTask/countTask
     * 类型：GET
     * 状态码：200
     */
    countTask: function (succ) {
        util.request('assignmentRunTask/countTask', 'GET', 'default', {}, succ)
    },
    /**
     * 获取【我负责】列表
     * 地址：assignmentRunTask/getHandleTasks
     * 类型：GET
     * 状态码：200
     */
    getHandleTasks: function (pageSize, current, param, succ) {
        util.request('assignmentRunTask/getHandleTasks?size=' + pageSize + '&current=' + current, 'GET', 'default', param, succ)
    },
    /**
     * 获取【我发起】列表
     * 地址：assignmentRunTask/getTasksCreateBy
     * 类型：GET
     * 状态码：200
     */
    getTasksCreateBy: function (pageSize, current, param, succ) {
        util.request('assignmentRunTask/getTasksCreateBy?size=' + pageSize + '&current=' + current, 'GET', 'default', param, succ)
    },
    /**
     * 获取【我参与】列表
     * 地址：assignmentRunTask/getParticipateTask
     * 类型：GET
     * 状态码：200
     */
    getParticipateTask: function (pageSize, current, param, succ) {
        util.request('assignmentRunTask/getParticipateTask?size=' + pageSize + '&current=' + current, 'GET', 'default', param, succ)
    },
    /**
     * 任务详情
     * 地址：assignmentRunTask/detail
     * 类型：GET
     * 状态码：200
     */
    getTaskDetail: function (param, succ) {
        util.request('assignmentRunTask/detail', 'GET', 'default', param, succ)
    },
    /**
     * 改变任务状态
     * 地址：assignmentRunTask/updateTaskStatus
     * 类型：PUT
     * 状态码：200
     */
    updateTaskStatus: function (param, succ) {
        util.request('assignmentRunTask/updateTaskStatus', 'PUT', 'default', param, succ)
    },
    /**
     * 任务日志
     * 地址：assignmentOperateLog/list
     * 类型：GET
     * 状态码：200
     */
    taskRelation: function (param, succ) {
        util.request('assignmentOperateLog/list', 'GET', 'default', param, succ)
    },
    //----------------------------------- 项目管理 ---------------------------------- 
    /**
     * 获取【项目】列表
     * 地址：assignmentRunProject/page
     * 类型：GET
     * 状态码：200
     */
    getProject: function (pageSize, current, param, succ) {
        util.request('assignmentRunProject/page?size=' + pageSize + '&current=' + current, 'GET', 'default', param, succ)
    },

    /**
     * 获取【项目】详情
     * 地址：assignmentRunProject/getNodeList
     * 类型：GET
     * 状态码：200
     */
    getNodeList: function (param, succ) {
        util.request('assignmentRunProject/getNodeList', 'GET', 'default', param, succ)
    },
    /**
     * 项目情况统计
     * 地址：assignmentRunProject/projectStatisticsMini
     * 类型：GET
     * 状态码：200
     */
    projectSum: function (param, succ) {
        util.request('assignmentRunProject/projectStatisticsMini', 'GET', 'default', param, succ)
    },
    /**
     * 个人项目统计
     * @param {*} succ 
     */
    getProjectCount: function (succ) {
        util.request('assignmentRunProject/projectStatisticsAll', 'GET', 'default', {}, succ);
    },
    //----------------------------------- 消息中心 ---------------------------------
    /**
     * 获取消息列表
     * 地址：msg.notice.page
     * 类型：GET
     * 状态码：200
     */
    noticePage: function (pageSize, current, param, succ) {
        util.request('msg.notice.page?size=' + pageSize + '&current=' + current, 'GET', 'default', param, succ)
    },
    /**
     * 消息统计
     * 地址：msg.notice.count
     * 类型：GET
     * 状态码：200
     */
    noticeCount: function (succ) {
        util.request('msg.notice.count', 'GET', 'default', {}, succ)
    },
    /**
     * 消息统计
     * 地址：msg.notice.markAsRead
     * 类型：PUT
     * 状态码：200
     */
    markAsRead: function (param, succ) {
        util.request('msg.notice.markAsRead', 'PUT', 'default', param, succ)
    },

    //登录接口【POST】
    loginpost: function (data) {
        var that = this;
        wx.request({
            url: that.globalData.server + 'auth/form',
            header: {
                // 'Authorization': 'Basic aml6aGktaW5zcGVjdGlvbi1taW5pOmppemhpLWluc3BlY3Rpb24tbWluaQ==',
                'Authorization': 'Basic aml6aGktcG1zLW1hcmtldC1taW5pLWFwaTpqaXpoaS1wbXMtbWFya2V0LW1pbmktYXBp',
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: "POST",
            data: data,
            success: function (res) {
                if (res.data.code == 200) {
                    wx.showToast({
                        title: "登录成功",
                        icon: 'none'
                    });
                    var loginuser = res.data.data;
                    wx.setStorageSync('user', loginuser);
                    that.getuserdata(loginuser.access_token);
                    // setTimeout(function () {
                    //   wx.redirectTo({
                    //     url: '/pages/home/home'
                    //   })
                    // }, 1300);
                } else {
                    wx.showToast({
                        title: "用户名或者密码错误",
                        icon: 'none'
                    })
                }
            }
        })
    },
    //获取登录人信息【GET】
    getuserdata: function (token) {
        var that = this;
        wx.request({
            url: that.globalData.server + 'sys.user.detail',
            header: {
                'Authorization': 'Bearer ' + token,
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: "GET",
            success: function (res) {
                wx.setStorageSync('userall', res.data.data);
                wx.reLaunch({
                    url: '/pages/index/index',
                })
            }
        })
    },
    // 签到接口【POST】new
    signin: function (data, token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                // url: that.globalData.server + 'ins.taskSign.post',
                url: that.globalData.server + 'ins.taskSign.create',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        wx.showToast({
                            title: "签到成功",
                            icon: 'none'
                        });
                        resolve(res.data);

                    }
                }
            })
        })
    },
    // 其他异常上传图片【POST】new
    ohter_uploadimg: function (taketype, token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.chooseImage({
                count: 1, // 默认9
                sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: taketype, // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    var tempFilePaths = res.tempFilePaths
                    var data = {
                        file: tempFilePaths[0]
                    }
                    wx.uploadFile({
                        url: that.globalData.server + 'ins.ohterExceptionTask.upload',
                        filePath: tempFilePaths[0],
                        name: 'file',
                        header: {
                            "Content-Type": "multipart/form-data",
                            'Authorization': 'Bearer ' + token,
                        },
                        formData: {
                            'user': 'test'
                        },
                        success: function (res) {
                            // var obj = JSON.parse(res.data);
                            resolve(res.data);
                        }
                    })
                }
            })
        })
    },
    //签到上传图片接口【POST】 New
    signin_uploadimg: function (taketype, token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.chooseImage({
                count: 1, // 默认9
                sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: taketype, // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    var tempFilePaths = res.tempFilePaths
                    var data = {
                        file: tempFilePaths[0]
                    }
                    wx.uploadFile({
                        url: that.globalData.server + 'file/formUpload',
                        filePath: tempFilePaths[0],
                        name: 'file',
                        header: {
                            "Content-Type": "multipart/form-data",
                            'Authorization': 'Bearer ' + token,
                        },
                        formData: {
                            'user': 'test'
                        },
                        success: function (res) {
                            var obj = JSON.parse(res.data);
                            resolve(obj);
                        }
                    })
                }
            })
        })
    },
    //更新头像【POST】New
    staffimg_change: function (taketype, token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.chooseImage({
                count: 1, // 默认9
                sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: taketype, // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    var tempFilePaths = res.tempFilePaths
                    var data = {
                        file: tempFilePaths[0]
                    }
                    wx.uploadFile({
                        url: that.globalData.server + 'sys.user.img.update',
                        filePath: tempFilePaths[0],
                        name: 'file',
                        header: {
                            "Content-Type": "multipart/form-data",
                            'Authorization': 'Bearer ' + token,
                        },
                        formData: {
                            'user': 'test'
                        },
                        success: function (res) {
                            var obj = JSON.parse(res.data);
                            resolve(obj);
                        }
                    })
                }
            })
        })
    },
    // 获取其他异常详细【GET】
    otherExceptionTaskDetail: function (token, data, url) {
        var that = this;

        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                data: data,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                success: function (res) {
                    resolve(res.data);
                }
            })
        })
    },
    // 新增每日报账单【POST】new
    dailybillSave: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'dailybill/save',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    // 新增其他异常【POST】new
    otherExceptionTask: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.otherExceptionTask.create',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    // 获取任务列表【GET】
    tasklist: function (token, data, obj, url) {
        var that = this;

        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                data: data,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                success: function (res) {
                    var pro = {};
                    pro.res = res.data.data;
                    pro.obj = obj
                    resolve(pro);
                }
            })
        })
    },
    //搜索、根据任务ID获取任务区域【通用。GET】
    getCommon: function (token, data, url) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                data: data,
                success: function (res) {
                    resolve(res);


                }
            })
        })
    },
    // 获取交接列表【GET】new
    getHandover: function (token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.mydept.staff.list',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    } else {
                        wx.showToast({
                            icon: 'none',
                            title: '网络出错',
                        })
                    }

                }
            })
        })
    },
    //巡场提交任务详情【PUT】new
    taskSubmit: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.task.submit',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    // if (res.data.code == 200) {
                    resolve(res.data);

                    // }
                }
            })
        })
    },
    //任务交接选人后提交【PUT】new
    taskRelief: function (token, data, url) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    //费用清单确认提交【PUT】new
    invoicesConfirm: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'invoices/confirm',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    //异常处理中-改变状态【PUT】new
    exceptionTaskHandleUpdate: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.exceptionTaskHandle.update',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    //巡场区域提交【PUT】new
    taskRegionSubmit: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.taskRegion.submit',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    //提交超时原因、任务-区域无异常提交【PUT】
    putCommon: function (token, data, url) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    // 审批详情【GET】
    approvalDetail: function (token, id) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'approval/approval.detail/' + id,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "GET",
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    // 任务-区域提交【有异常时】【POST】
    exceptionTask: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.ExceptionTask.create',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    // 提交审批申请【POST】
    approvalCreate: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'approval/approval.create',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "POST",
                data: data,
                success: function (res) {
                    if (res.data.code == 200) {
                        resolve(res.data);
                    }
                }
            })
        })
    },
    //审批操作【PUT】new
    approvalUpdate: function (token, data) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'approval/approval.update',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/json"
                },
                method: "PUT",
                data: data,
                success: function (res) {
                    resolve(res.data);
                }
            })
        })
    },
    // 获取消息列表【GET】
    taskNoticeList: function (token, data, obj, url) {
        var that = this;

        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + url,
                data: data,
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                success: function (res) {
                    var pro = {};
                    pro.res = res.data.data;
                    pro.obj = obj;
                    pro.statusCode = res.statusCode;
                    resolve(pro);
                }
            })
        })
    },
    // 巡场完成信息【GET】
    getTaskInfo: function (token) {
        var that = this;
        return new Promise(function (resolve) {
            wx.request({
                url: that.globalData.server + 'ins.task.info',
                header: {
                    'Authorization': 'Bearer ' + token,
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                method: "GET",
                success: function (res) {
                    resolve(res.data);
                }
            })
        })
    },
};

module.exports = api;