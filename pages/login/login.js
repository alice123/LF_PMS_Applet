var app = getApp();
Page({
  data: {
    username: '',
    password: '',
    isMask:false
  },
  onLoad: function (options) {

  },
  formSubmit: function (e) {

    var that = this;
    var inputval = e.detail.value;
    if (!inputval.username) {
      wx.showToast({
        title: "用户名不能为空",
        icon: 'none'
      });
      return;
    }
    if (!inputval.password) {
      wx.showToast({
        title: "密码不能为空",
        icon: 'none'
      });
      return;
    }
    wx.showToast({
      title: "登录中",
      icon: 'loading'
    })
    var user = {
      username: inputval.username,
      password: inputval.password
    }
    //登录接口
    app.api.loginpost(user);
  },
  forgotPwd:function(){
    this.setData({
      isMask:true
    })
  },
  close:function(){
    this.setData({
      isMask:false
    })
  }

})