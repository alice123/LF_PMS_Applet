const app = getApp()
var util = require('../../utils/util.js');
Page({
  data: {
    currentTab:"0",
    tabList:[
      {
        id:"0",
        title:'我负责的',
      },
      {
        id:"1",
        title:'我发起的',
      },
      {
        id:"2",
        title:'我参与的',
      }
    ],
    currentTotalPage: 0,
    pageCurrent:1,
    current: '0',
    handleTasksList: [],//我负责的
    createByList: [],//我发起的
    participateTaskList: [],//我参与的
    queryType:'',//上一页是什么状态【今日任务，逾期任务，预期任务】
  },
  onLoad: function (options) {
    var that = this;
    console.log(options)
    that.setData({
      queryType:options.queryType
    })
    wx.setNavigationBarTitle({title: options.name});
    var param = {queryType:options.queryType}
    app.api.getHandleTasks(10,1,param,that.getList)
  },
  //获取列表
  getList:function(res){
    var that = this;
    var totalPage = Math.ceil(res.data.total / 10);
    if (that.data.pageCurrent <= totalPage){
      that.data.pageCurrent += 1;
      var newDataList = [];
      var serverDataList = res.data.records;
      console.log("serverDataList------")
      console.log(serverDataList)
      for (var idx in serverDataList) {
        var serverData = serverDataList[idx];
        var tmp = {
          endTime: serverData.endTime,
          handlerName: serverData.handlerName?serverData.handlerName:'',
          nodeName: serverData.nodeName,
          priority: serverData.priority,
          projectEndTime: serverData.projectEndTime,
          projectName: serverData.projectName,
          projectStartTime: serverData.projectStartTime,
          projectStatus: serverData.projectStatus,
          startTime: serverData.startTime,
          taskId: serverData.taskId,
          taskName: serverData.taskName,
          taskStatus: serverData.taskStatus
        }
        newDataList.push(tmp)
      }
      if (that.data.currentTab == '0') {
        that.setData({
          handleTasksList: that.data.handleTasksList.concat(newDataList)
        })
      } else if (that.data.currentTab == '1') {
        that.setData({
          createByList: that.data.createByList.concat(newDataList)
        })
      } else if (that.data.currentTab == '2') {
        that.setData({
          participateTaskList: that.data.participateTaskList.concat(newDataList)
        })
      }
    }
    wx.stopPullDownRefresh();
  },
  
  choice:function(e){
    var that = this;
    var currentTab = e.currentTarget.dataset.id;
    console.log("id======"+currentTab)
    that.setData({
      currentTab:currentTab,
      pageCurrent:1
    })
    switch(currentTab) {
      case '0':
        console.log("我负责的")
        that.data.handleTasksList = [];
        var param = {queryType:that.data.queryType}
        app.api.getHandleTasks(10,1,param,that.getList);
        break;
      case '1':
        console.log("我发起的")
        that.data.createByList = [];
        var param = {queryType:that.data.queryType}
        app.api.getTasksCreateBy(10,1,param,that.getList);
        break;
        
      default:
        console.log("我参与的")
        that.data.participateTaskList = [];
        var param = {queryType:that.data.queryType}
        app.api.getParticipateTask(10,1,param,that.getList);
    }
  },
  godetail:function(e){
    var that = this;
    var taskid = e.currentTarget.dataset.taskid;
    console.log(taskid)
    wx.navigateTo({
      url: '/pages/task_detail/task_detail?taskid='+taskid,
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    console.log(that.data.currentTab)
    switch (that.data.currentTab) {
      case '0':
        var param = {queryType:that.data.queryType}
        app.api.getHandleTasks(10,that.data.pageCurrent,param,that.getList);
        break;
      case '1':
        var param = {queryType:that.data.queryType}
        app.api.getTasksCreateBy(10,that.data.pageCurrent,param,that.getList);
        break;
      case '3':
        var param = {queryType:that.data.queryType}
        app.api.getParticipateTask(10,that.data.pageCurrent,param,that.getList);
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})