var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    goodsType: [{
      value: 1,
      label: '未开始',
    }, {
      value: 2,
      label: '进行中',
    }, {
      value: 3,
      label: '已完成',
    }, {
      value: 4,
      label: '已中止',
    }],
    taskId:'',
    value3:[],
    taskStatus:0,
    taskStatusName:'',//状态
    taskName:'',//任务名称
    taskList:[],//参与人，时间，项目，备注
    priority:0,//优先级【1：较低 2：普通 3：较高 4：非常紧急】
    taskRelation:[],//关联任务
    budget:'59999',//项目预算
    taskChild:[],//子任务列表
    taskRecord:[],//任务日志列表
  },
  onLoad: function (options) {
    var that = this;
    console.log(options)
    var param = {
      taskId:options.taskid
    };
    app.api.getTaskDetail(param,function(res){
      console.log(res)
      var obj = res.data;
      
      that.setData({
        taskName:obj.taskName,
        taskStatus:obj.taskStatus,
        taskStatusName:that.data.goodsType[obj.taskStatus-1].label?that.data.goodsType[obj.taskStatus-1].label:'',
        priority:obj.priority,
        handlerName:obj.handlerName,
        startTime: obj.startTime?util.formatShortTime(obj.startTime):'',
        endTime: obj.endTime?util.formatShortTime(obj.endTime):'',
        subTasks:obj.subTasks,
        projects:obj.projects,
        formInfo:obj.form?JSON.parse(obj.form.content):'',
        taskId:options.taskid
      })
    })
    var obj = {sourceId:options.taskid}
    app.api.taskRelation(obj,function(res){
      console.log("1111")
      console.log(res)
     
      if(res.code==200){
        var taskRecord = res.data;
        if(0<taskRecord.length){
          for(var i in taskRecord){
            taskRecord[i].createTime=util.formatShortTime(taskRecord[i].createTime)
          }
        }
        that.setData({
          taskRecord:taskRecord
        })
      }
      
    })
  },
  onClickGoodsType:function(e){
    var that = this;
    that.setData({ visible: true })
  },
  onConfirm:function(e) {
    this.setValue(e.detail)
    console.log(e.detail)
  },
  setValue:function(values) {
    var that = this;  
    that.setData({
      value3:values.value,
      taskStatus: values.value,
      taskStatusName: values.label,
    })
    var param = {
      id:that.data.taskId,
      taskStatus:values.value[0]
    };
    
    app.api.updateTaskStatus(param,function(res){
      console.log(res)
      if(res.code!=200){
        wx.showToast({
          title: res.msg,
          icon:'none'
        })
      }
    })
  },
  
  onVisibleChange: function(e) {
    var that = this;
    that.setData({ visible: e.detail.visible })
  },
  onShareAppMessage: function () {

  }
})