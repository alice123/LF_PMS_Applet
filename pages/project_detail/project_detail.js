var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    steps:[],
    currentCho:'',//当前点击的ID
    myTaskName:'',
    handlerName:'',
    startTime:'',
    endTime:'',
    todayToDo:'',
    totalTasks:'',
    finished:'',
    overtime:'',
  },
  onLoad: function (options) {
    var that = this;
    that.setData({
      myTaskName:options.myTaskName,
      handlerName:options.handlerName,
      startTime:options.startTime,
      endTime:options.endTime,
      todayToDo:options.todayToDo,
      totalTasks:options.totalTasks,
      finished:options.finished,
      overtime:options.overtime
    })
    var param = {
      projectId:options.id
    };
    app.api.getNodeList(param,that.getNodeList)
  },
  getNodeList:function(res){
    var that = this;
    var list = res.data;
    for(var i = 0;i<list.length;i++){
      for(var j = 0;j<list[i].tasks.length;j++){
        list[i].tasks[j].startTime=list[i].tasks[j].startTime?util.formatShortTime(list[i].tasks[j].startTime):'',
        list[i].tasks[j].endTime=list[i].tasks[j].endTime?util.formatShortTime(list[i].tasks[j].endTime):''
      }
    }
    
    that.setData({
      steps:list,
    })
  },
  choice:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    var steps = that.data.steps;
    for(var i = 0;i<steps.length;i++){
      if(steps[i].id==id){
        steps[i].isshow=!steps[i].isshow;
      }
    }
    that.setData({
      currentCho:id,
      steps:steps
    })
  }
})