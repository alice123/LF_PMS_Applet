var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    isShowCho:false,
    currentCho:'活动管理',
    groupCode:1,
    typeList:[
      {id:1,label:'活动管理'},
      {id:2,label:'TC筹开店铺'},
      {id:3,label:'计划管理'},
    ],
    currentTab:1,
    tabList:[
      {id:1,title:'进行中'},
      {id:2,title:'已完成'},
      {id:3,title:'已逾期'},
      {id:4,title:'已中止'}
    ],
    notStartList:[],
    processingList:[],
    finishedList:[],
    overtimeList:[],
    currentTotalPage: 0,
    pageCurrent:1,
  },
  onLoad: function (options) {
    var that = this;
    var param = {
      queryType:1,
      groupCode:1
    };
    app.api.getProject(10,1,param,that.getProject);
  },
  getProject:function(res){
    var that = this;
    var totalPage = Math.ceil(res.data.total / 10);
    
    if (that.data.pageCurrent <= totalPage){
      that.data.pageCurrent += 1;
      var newDataList = [];
      var serverDataList = res.data.records;
      console.log("serverDataList------")
      console.log(serverDataList)
      for (var idx in serverDataList) {
        var serverData = serverDataList[idx];
        var tmp = {
          endTime: util.formatShortTime(serverData.endTime),
          finished: serverData.finished,
          handlerName: serverData.handlerName?serverData.handlerName:'',
          id: serverData.id,
          overtime: serverData.overtime,
          processing: serverData.processing,
          projectName: serverData.projectName,
          projectStatus: serverData.projectStatus,
          startTime: util.formatShortTime(serverData.startTime),
          todayToDo: serverData.todayToDo,
          undo: serverData.undo,
          totalTasks:serverData.totalTasks
        }
        newDataList.push(tmp)
      }
      
      if (that.data.currentTab == 1) {
        that.setData({
          notStartList: that.data.notStartList.concat(newDataList)
        })
      } else if (that.data.currentTab == 2) {
        that.setData({
          processingList: that.data.processingList.concat(newDataList)
        })
      } else if (that.data.currentTab == 3) {
        that.setData({
          finishedList: that.data.finishedList.concat(newDataList)
        })
      } else if (that.data.currentTab == 4) {
        that.setData({
          overtimeList: that.data.overtimeList.concat(newDataList)
        })
      }
    }
    wx.stopPullDownRefresh();
    
  },
  indexCho:function(){
    this.setData({
      isShowCho:!this.data.isShowCho
    })
  },
  choice:function(e){
    var that = this;
    var groupCode = e.currentTarget.dataset.id;
    var idx = e.currentTarget.dataset.idx;
    
    that.setData({
      currentCho:that.data.typeList[idx].label,
      groupCode:groupCode,
      notStartList:[],
      processingList:[],
      finishedList:[],
      overtimeList:[],
      pageCurrent:1
    })
    var param = {
      queryType:that.data.currentTab,
      groupCode:groupCode
    };
    console.log(param)
    app.api.getProject(10,1,param,that.getProject);
  
  },
  projectChoice:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    that.setData({
      currentTab:id,
      pageCurrent:1,
      notStartList:[],
      processingList:[],
      finishedList:[],
      overtimeList:[]
    })
    var param = {
      queryType:id,
      groupCode:that.data.groupCode
    };
    app.api.getProject(10,1,param,that.getProject)
    
  },
  godetail:function(e){
    var that = this;
    
    var id = e.currentTarget.dataset.id;
    var myTaskName = e.currentTarget.dataset.name;
    var todayToDo = e.currentTarget.dataset.todaytodo;
    var overtime = e.currentTarget.dataset.overtime;
    var finished = e.currentTarget.dataset.finished;
    var handlerName = e.currentTarget.dataset.handlername;
    var totalTasks = e.currentTarget.dataset.totaltasks;
    var startTime = e.currentTarget.dataset.starttime;
    var endTime = e.currentTarget.dataset.endtime;
    console.log( e.currentTarget.dataset)
    wx.navigateTo({
      url: '/pages/project_detail/project_detail?id='+id+'&myTaskName='+myTaskName+'&todayToDo='+todayToDo+'&overtime='+overtime+'&finished='+finished+'&handlerName='+handlerName+'&totalTasks='+totalTasks+'&startTime='+startTime+'&endTime='+endTime,
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    console.log(that.data.pageCurrent)
    var param = {
      queryType:that.data.currentTab,
      groupCode:that.data.groupCode
    };
    app.api.getProject(10,that.data.pageCurrent,param,that.getProject)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})