var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    approvalChild:[],
    approvalOperation:[],
    ismine:false,
    imgList:[],//上传附件
    taskId:'',
    id:'',
    formId:'',
    isProject:false,
    handlerName:'',//负责人
    createTime:'',
    endTime:'',
    priority:'',//优先级
    projectStatus:'',//项目状态
    isshowapp:false,//批注
    isRepeat:false,//重复提交
  },
  onLoad: function (options) {
    var that = this;
    console.log(options)

    
    if(options.currentTabs=='1'){
      if(options.reject=='0'){
        that.setData({
          ismine:true,
        })
      }else{
        that.setData({
          isRepeat:true
        })
      }
     
    }
    that.setData({
      taskId:options.taskId,
      id:options.id,
      formId:options.formId
    })
    app.api.getProcessDetail(options.formId,that.getProcessDetail)
  },
 
  getProcessDetail:function(res){
    var that = this;
    console.log(res)
    if(res.code==200){
      if(res.data.sourceObject){
        var sourceObject = JSON.parse(res.data.sourceObject);
        console.log(sourceObject.id)
        app.api.getProcessingProject(sourceObject.id,function(res){
          console.log("======项目详情")
          console.log(res)
          that.setData({
            isProject:true,
            handlerName:res.data.handlerName,
            startTime:res.data.startTime?util.formatShortTime(res.data.startTime):'',
            endTime:res.data.endTime?util.formatShortTime(res.data.endTime):'',
            priority:res.data.priority,
            projectStatus:res.data.projectStatus
          })
        })
      }
      
      var list = JSON.parse(res.data.content);
      
    
    console.log(list)
    if(that.data.ismine){
      that.setData({
        templateName:res.data.templateName,
        approvalList:list,
        
      })
    }else{
      that.setData({
        templateName:res.data.templateName,
        approvalList:list
      })
    }
    }else{
      console.log(res.msg)
    }
  },
  previewImage:function(e){
    var current = e.target.dataset.src;
    console.log(current)
    wx.previewImage({
      current: current, // 当前显示图片的http链接  
      urls: this.data.imgList // 需要预览的图片http链接列表  
    }) 
  
  },
  agreeRep:function(){
    var that = this;
    var param = {
      taskId:that.data.taskId,
      saveEntity:true,
      bizId:that.data.id,
      isAgree:true
    };
    app.api.putProcess(param,that.putProcess)
  },
  disagreeRep:function(){
    var that = this;
    var param = {
      taskId:that.data.taskId,
      saveEntity:true,
      bizId:that.data.id,
      isAgree:false
    };
    app.api.putProcess(param,that.putProcess)
  },
  agree:function(){
    var that = this;
    that.setData({
      isAgree:true,
      isshowapp:true
    })
  },
  disagree:function(){
    var that = this;
    that.setData({
      isAgree:false,
      isshowapp:true
    })
  },
  reasonInput:function(e){
    var that = this;
    that.setData({
      reason: e.detail.value
    })
  },
  // 操作原因提交
  approvalDone:function(e){
    var that = this;
    if(!that.data.isAgree){
      if(!that.data.reason){
        wx.showToast({
          title: '请填写驳回原因',
          icon:'none'
        })
        return false;
      }
      var param = {
        taskId:that.data.taskId,
        saveEntity:true,
        bizId:that.data.id,
        isAgree:false,
        comment:that.data.reason
      };
    }else{
      if(!that.data.reason){
        wx.showToast({
          title: '请填写审批意见',
          icon:'none'
        })
        return false;
      }
      var param = {
        taskId:that.data.taskId,
        saveEntity:true,
        bizId:that.data.id,
        isAgree:true,
        comment:that.data.reason
      };
    }
    app.api.putProcess(param,that.putProcess)
  },
  approvalCancle:function(){
    var that = this;
    that.setData({
      isshowapp: false,
      reason:''
    })
  },
  putProcess:function(res){
    console.log(res)
    if(res.code==200){
      var pages = getCurrentPages();
      var currPage = pages[pages.length - 1];   //当前页面
      var prevPage = pages[pages.length - 2];  //上一个页面

      wx.navigateBack({
        delta: 1,
        success: function () {
          prevPage.init();
        }
      })
    }
  }
})