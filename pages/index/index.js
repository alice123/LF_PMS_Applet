const app = getApp();
const api = app.api;
var util = require('../../utils/util.js');
Page({
  data: {
    gridBlock: [], //顶部菜单
    progressList: {
      1: {
        name: '活动管理',
        img: '../../img/icon/index05.png'
      },
      2: {
        name: 'TC筹开店铺',
        img: '../../img/icon/index05.png'
      },
      3: {
        name: '计划管理',
        img: '../../img/icon/index05.png'
      }
    },
    progressIndex: 1,
    progressName: '',
    finished: '0', //完成
    processing: '0', //进行中
    discontinue: '0', //中止
    overtime: '0', //逾期
    allSum: '0', //总数
    myTask: [], //我的任务
    myApproval: [], //我的审批
    isShowCho: false,
    isAdmin: false,
    finishedPer: '0',
    overtimePer: '0',
    discontinuePer: '0',
    isMaskHidden: false,
    msgSum: '0', //我的消息数
    approvalSum: '0', //我的审批数
    navrole: '0',
    taskSum: '0',
    isLoginShow: false,
    loginBool: {
      isLogin: true,
      isTel: false,
      noLogin: false,
    }
  },
  onLoad: function () {
    console.log('onLoad')
    wx.hideTabBar({});
    var that = this;
    var user = wx.getStorageSync('user');
    if (user.roles == 'admin') {
      var gridBlock = [{
          name: '项目管理',
          icon: '../../img/icon/index011.png',
          path: '/pages/project_manage/project_manage'
        },
        {
          name: '多经管理',
          icon: '../../img/icon/index022.png',
          path: '/pages/economic_manage_admin/economic_manage_admin',
          //如果是管理员，则进去 '/pages/economic_manage_admin/economic_manage_admin'
        },
        {
          name: '我的任务',
          icon: '../../img/icon/index033.png',
          path: '/pages/my_task_status/my_task_status'
        },
        {
          name: '我的审批',
          icon: '../../img/icon/index044.png',
          path: '/pages/approval/approval'
        }
      ];
    } else {
      var gridBlock = [{
          name: '项目管理',
          icon: '../../img/icon/index011.png',
          path: '/pages/project_manage/project_manage'
        },
        {
          name: '多经管理',
          icon: '../../img/icon/index022.png',
          path: '/pages/economic_manage/economic_manage'
        },
        {
          name: '我的任务',
          icon: '../../img/icon/index033.png',
          path: '/pages/my_task_status/my_task_status'
        },
        {
          name: '我的审批',
          icon: '../../img/icon/index044.png',
          path: '/pages/approval/approval'
        }
      ];
    }
    that.initLogin();
    var myTask = [];
    that.setData({
      gridBlock: gridBlock,
      myTask: myTask
    })

  },
  onShow: function () {
    console.log('show')
    let _this = this;
    // 数据预览
    _this.getProgressDetail();
    // 我的信息统计
    _this.getMsgCount();
    // 我的任务
    _this.getTasksList();
    // 我的审批
    _this.getProgressDetail();
  },
  handleMove: function () {},
  /**
   * 获取用户信息
   * @param {*} e 
   */
  getUserInfo(e) {
    console.log(e);
    let _this = this;
    app.globalData.encryptedData = e.detail.encryptedData;
    app.globalData.iv = e.detail.iv;

    api.register({
      encryptedData: e.detail.encryptedData,
      iv: e.detail.iv
    }, _this.register);
  },
  register: function (res) {
    var that = this;
    console.log(app.globalData)
    if (res.code == 200) {
      that.setData({
        ['loginBool.isLogin']: false,
        ['loginBool.isTel']: true,
        ['loginBool.noLogin']: false,
      })
    } else {
      that.setData({
        ['loginBool.isLogin']: false,
        ['loginBool.isTel']: true,
        ['loginBool.noLogin']: false,
      })
    }
  },
  // 微信授权手机号码登录
  getPhoneNumber: function (e) {
    var that = this;
    if (e.detail.encryptedData == undefined) {

    } else {
      app.api.getPhone({
        code: app.globalData.code,
        iv: e.detail.iv,
        encryptedData: e.detail.encryptedData
      }, (res) => {
        console.log('解密后的电话号码****************************', res.data.phoneNumber)
        app.globalData.mobile = res.data.phoneNumber;
        app.globalData.iv = e.detail.iv;
        wx.setStorageSync('phoneNumber', res.data.phoneNumber);
        that.setData({
          ['loginBool.isLogin']: false,
          ['loginBool.isTel']: false,
          ['loginBool.noLogin']: false,
          isLoginShow: false,
        })
        that.initLogin();

      })
    }
  },
  /**
   * 我的审批
   * @param {*} res 
   */
  getProcessingDetail: function () {
    let _this = this;
    api.getProcessing(3, 1, res => {
      if (res.code == 200) {
        var myApproval = res.data.records;
        if (myApproval.lenth != 0) {
          for (var idx in myApproval) {
            console.log(myApproval[idx])
            myApproval[idx].createTime = myApproval[idx].createTime ? util.formatShortTime(myApproval[idx].createTime) : ''
          }
        }
        _this.setData({
          myApproval: myApproval,
          approvalSum: res.data.total ? res.data.total : '0'
        })
      }
    });
  },
  initLogin: function () {
    var that = this;
    wx.login({
      success(res) {
        if (res.code) {
          var data = {
            code: res.code
          };
          app.api.authorize(data).then(function (res) {
            console.log("查看token:")
            console.log(res)
            if (res.code == 200) {
              // 已授权，用token获取个人信息
              wx.setStorageSync('token', res.data.access_token);
              app.api.userDetail(function (res) {
                that.setData({
                  navrole: res.data.isManager == 1 ? 1 : 0
                });

                // 数据预览
                that.getProgressDetail();
                // 我的信息统计
                that.getMsgCount();
                // 我的任务
                that.getTasksList();
                // 我的审批
                that.getProcessingDetail();

              });
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
          return false;
        }
      }
    })
  },
  gonews: function () {
    wx.navigateTo({
      url: '/pages/notice/notice',
    })
  },
  choice: function (e) {
    let that = this;
    let data = e.currentTarget.dataset;

    that.setData({
      progressIndex: Number(data.id),
      progressName: data.name,
      isShowCho: !this.data.isShowCho,
      isMaskHidden: !this.data.isMaskHidden
    })

    that.getProgressDetail();
  },
  indexCho: function () {
    this.setData({
      isShowCho: !this.data.isShowCho,
      isMaskHidden: !this.data.isMaskHidden
    })
  },
  /**
   * 取消登录提示
   */
  goLoginTips: function () {
    this.setData({
      isLoginShow: false,
    })
  },
  gopath: function (e) {
    var that = this;
    var path = e.currentTarget.dataset.path;
    wx.navigateTo({
      url: path,
    })
  },
  goMoreTask: function () {
    wx.navigateTo({
      url: '/pages/my_task_status/my_task_status',
    })
  },
  goMoreApproval: function () {
    wx.navigateTo({
      url: '/pages/approval/approval',
    })
  },
  goTaskDetail: function (e) {
    var taskid = e.currentTarget.dataset.taskid;
    wx.navigateTo({
      url: '/pages/task_detail/task_detail?taskid=' + taskid,
    })
  },
  /**
   * 获取任务列表
   */
  getTasksList() {
    let _this = this;
    var param = {
      queryType: 1
    }
    api.getHandleTasks(3, 1, param, function (res) {
      var myTask = res.data.records || [];
      for (var idx in myTask) {
        console.log(myTask[idx])
        myTask[idx].startTime = myTask[idx].startTime ? util.formatShortTime(myTask[idx].startTime) : '',
          myTask[idx].endTime = myTask[idx].endTime ? util.formatShortTime(myTask[idx].endTime) : ''
      }
      _this.setData({
        myTask: myTask,
        taskSum: res.data.total
      })
    })
  },
  /**
   * 获取我的信息统计
   */
  getMsgCount() {
    let _this = this;
    api.noticeCount(function (res) {
      if (res.data) {
        _this.setData({
          msgSum: res.data.unreadCount ? res.data.unreadCount : '0'
        })
      }
    })
  },
  /**
   * 获取数据预览
   */
  getProgressDetail() {
    let _this = this;
    api.projectSum({
      groupCode: _this.data.progressIndex
    }, function (res) {
      if (res.code == 401) {
        _this.setData({
          isLoginShow: true,
        })
      }
      if (res.code != 200) {
        return;
      }
      _this.setData({
        finished: res.data.finished ? res.data.finished : 0,
        processing: res.data.processing ? res.data.processing : 0,
        overtime: res.data.overtime ? res.data.overtime : 0,
        discontinue: res.data.discontinue ? res.data.discontinue : 0,
        finishedPer: util.MathMui(util.MathDiv(res.data.finished, res.data.total), 100),
        overtimePer: util.MathMui(util.MathDiv(res.data.overtime, res.data.total), 100),
        discontinuePer: util.MathMui(util.MathDiv(res.data.discontinue, res.data.total), 100),
        allSum: res.data.total || 0,
      })
    })
  },
  goApprovalDetail: function (e) {
    var that = this;
    var formId = e.currentTarget.dataset.formid;
    var taskId = e.currentTarget.dataset.taskid;
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/approval_detail/approval_detail?formId=' + formId + '&currentTabs=1' + '&taskId=' + taskId + '&id=' + id,
    })
  },
  //阻止苹果下拉出现空白
  onPageScroll: function (e) {
    if (e.scrollTop < 0) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    }
  }
})