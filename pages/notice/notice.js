var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    noticeType: "0",
    tabList: [{
        id: "0",
        title: '全部'
      },
      {
        id: "1",
        title: '项目'
      },
      {
        id: "2",
        title: '任务'
      },
      {
        id: "3",
        title: '审批'
      },
    ],
    isRead: '0',
    statusList: [{
        id: '0',
        title: '未读',
        img: '../../img/icon/notice01.png'
      },
      {
        id: '1',
        title: '已读',
        img: '../../img/icon/notice02.png'
      }
    ],
    currentTotalPage: 0,
    pageCurrent: 1,
    allNewsList: [], //【全部】列表
    projectNewsList: [], //【项目】列表
    taskNewsList: [], //【任务】列表
    approvalNewsList: [], //【审批】列表
    hasOnShow: false
  },
  onLoad: function (options) {
    var that = this;
    that.init()
  },
  init: function () {
    var that = this;
    var param = {
      isRead: '0'
    };
    that.setData({
      noticeType: '0',
      isRead: '0'
    })
    app.api.noticePage(10, 1, param, that.noticePage)
  },
  //获取列表
  noticePage: function (res) {
    var that = this;
    var totalPage = Math.ceil(res.data.total / 10);
    if (that.data.pageCurrent <= totalPage) {
      that.data.pageCurrent += 1;
      var newDataList = [];
      var serverDataList = res.data.records;
      for (var idx in serverDataList) {
        var serverData = serverDataList[idx];
        var tmp = {
          bizId: serverData.bizId,
          content: serverData.content,
          createTime: serverData.createTime ? util.shortTime(serverData.createTime) : '',
          id: serverData.id,
          isRead: serverData.isRead,
          noticeType: serverData.noticeType,
          title: serverData.title
        }
        newDataList.push(tmp)
      }
      if (that.data.noticeType == '0') {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          allNewsList: that.data.allNewsList.concat(newDataList)
        })
      } else if (that.data.noticeType == '1') {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          projectNewsList: that.data.projectNewsList.concat(newDataList)
        })
      } else if (that.data.noticeType == '2') {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          taskNewsList: that.data.taskNewsList.concat(newDataList)
        })
      } else {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          approvalNewsList: that.data.approvalNewsList.concat(newDataList)
        })
      }
    }
    wx.stopPullDownRefresh();
  },
  //选择消息类型
  choiceType: function (e) {
    var that = this;
    var noticeType = e.currentTarget.dataset.id;
    that.setData({
      noticeType: noticeType,
      pageCurrent: 1,
      allNewsList: [],
      projectNewsList: [],
      taskNewsList: [],
      approvalNewsList: [],
    })
    var param = {};
    if (noticeType == '0') {
      param = {
        isRead: that.data.isRead
      }
    } else {
      param = {
        noticeType: noticeType,
        isRead: that.data.isRead
      }
    }
    app.api.noticePage(10, 1, param, that.noticePage)
  },
  //选择【已读】or【未读】
  choiceStatus: function (e) {
    var that = this;
    var isRead = e.currentTarget.dataset.id;

    that.setData({
      isRead: isRead,
      pageCurrent: 1,
      allNewsList: [],
      projectNewsList: [],
      taskNewsList: [],
      approvalNewsList: [],
    })
    var param = {};
    if (that.data.noticeType == '0') {
      param = {
        isRead: isRead
      }
    } else {
      param = {
        noticeType: that.data.noticeType,
        isRead: isRead
      }
    }
    app.api.noticePage(10, 1, param, that.noticePage)
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if (that.data.noticeType == '0') {
      var param = {
        isRead: that.data.isRead
      }
      app.api.noticePage(10, that.data.pageCurrent, param, that.noticePage)
    } else {
      var param = {
        isRead: that.data.isRead,
        noticeType: that.data.noticeType
      }
      app.api.noticePage(10, that.data.pageCurrent, param, that.noticePage)
    }
  },
  //操作为【已读】
  goread: function (e) {
    var that = this;
    var param = {
      id: e.currentTarget.dataset.id
    }
    that.setData({
      pageCurrent: 1,
      allNewsList: [],
      projectNewsList: [],
      taskNewsList: [],
      approvalNewsList: []
    })
    app.api.markAsRead(param, function (res) {
      if (res.code == 200) {
        var mydata = {};
        if (that.data.noticeType == '0') {
          mydata = {
            isRead: that.data.isRead
          }
          app.api.noticePage(10, 1, mydata, that.noticePage)
        } else {
          mydata = {
            noticeType: that.data.noticeType,
            isRead: that.data.isRead
          }
          app.api.noticePage(10, 1, mydata, that.noticePage)
        }

      } else {
        wx.showToast({
          title: res.msg,
          icon: 'none'
        })
      }
    })


  },
  // markAsRead
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})