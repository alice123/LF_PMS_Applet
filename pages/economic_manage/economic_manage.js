var app = getApp();
Page({
  data: {
    pageCurrent: 1,
    searchKey: '',
    displayState: '0', //[0]可租赁  [1]已租赁
    rentStatus: [{
        label: '空置中',
        id: '1'
      },
      {
        label: '今天到期',
        id: '0'
      },
      {
        label: '近7天到期',
        id: '7'
      },
      {
        label: '近30天到期',
        id: '30'
      },
    ],
    isRent: false,
    warningDays: '1',
    currentCho: '点位类型',
    pointTypeList: [],
    displayType: '', //点位类型
    currentList: [],
    isSearch: false,
    searchList: []
  },
  onLoad: function (options) {
    var that = this;
    // 获取【点位类型】
    app.api.getDisplayType(function (res) {
      // console.log(res)
      that.setData({
        pointTypeList: res.data
      })
      that.initList();
      // console.log(that.data.pointTypeList[0].id)
    })
  },
  initList: function () {
    var that = this;
    var param = {
      isAdmin: '0',
      displayState: 0
    };
    app.api.getDisplayList(10, 1, param, that.getDisplayList)
  },
  getDisplayList: function (res) {
    var that = this;
    console.log("===============获取的列表")
    console.log(res)

    var totalPage = Math.ceil(res.data.total / 10);
    if (that.data.pageCurrent <= totalPage) {
      that.data.pageCurrent += 1;
      var newDataList = [];
      var serverDataList = res.data.records;
      for (var idx in serverDataList) {
        var obj = serverDataList[idx];
        var tmp = {
          area: obj.area ? obj.area : '0',
          deptId: obj.deptId,
          deptName: obj.deptName,
          displayNo: obj.displayNo,
          displayPicUrl: obj.displayPicUrl,
          displayRank: obj.displayRank,
          displaySize: obj.displaySize,
          displayState: obj.displayState,
          displaySubType: obj.displaySubType,
          displaySubTypeName: obj.displaySubTypeName,
          displayType: obj.displayType,
          endTime: obj.endTime,
          id: obj.id,
          mobile: obj.mobile,
          position: obj.position,
          staffId: obj.staffId,
          staffName: obj.staffName ? obj.staffName : '',
          startTime: obj.startTime,
          terminateTime: obj.terminateTime,
          warningDays: obj.warningDays,
        };
        newDataList.push(tmp)
      }
      console.log(that.data.isSearch)
      if (that.data.isSearch) {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          searchList: that.data.searchList.concat(newDataList)
        })
      } else {
        that.setData({
          pageCurrent: that.data.pageCurrent,
          currentList: that.data.currentList.concat(newDataList)
        })
      }

    }
    wx.stopPullDownRefresh();
  },
  searchInput: function (e) {
    // console.log(e.detail.value)
    var that = this;
    that.setData({
      searchKey: e.detail.value
    })
  },
  gosearch: function () {
    // console.log("去搜索")
    var that = this;
    if (!that.data.searchKey) {
      that.setData({
        isSearch: false,
        searchList: [],
        pageCurrent: 1
      })
    } else {
      that.setData({
        isSearch: true,
        pageCurrent: 1
      })
      var param = {
        isAdmin: 0,
        position: that.data.searchKey
      }
      app.api.getDisplayList(10, 1, param, that.getDisplayList);

    }

  },
  // 预览图片
  perViewPic: function (e) {
    var that = this;
    var src = e.currentTarget.dataset.url; //获取data-src
    var imgList = [e.currentTarget.dataset.url]; //获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },
  // 可租，已租
  choRent: function (e) {
    var that = this;
    var displayState = e.currentTarget.dataset.id;
    console.log(displayState)
    if (displayState == 0) {
      //可租
      var param = {
        isAdmin: 0,
        displayState: displayState,
        displayType: that.data.displayType,
        warningDays: that.data.warningDays
      };
      app.api.getDisplayList(10, 1, param, that.getDisplayList);
      that.setData({
        isRent: false,
        pageCurrent: 1,
        currentList: [],
        displayState: displayState,
        displayType: that.data.displayType,
        warningDays: that.data.warningDays
      })
    } else {
      //已出租
      var param = {
        isAdmin: 0,
        displayState: displayState,
        displayType: that.data.displayType,
      };
      app.api.getDisplayList(10, 1, param, that.getDisplayList);
      that.setData({
        isRent: true,
        pageCurrent: 1,
        currentList: [],
        displayState: displayState,
        displayType: that.data.displayType
      })
    }
  },
  // 空置？今天到期？
  choStatus: function (e) {
    var that = this;
    var warningDays = e.currentTarget.dataset.id;
    var param = {
      isAdmin: 0,
      warningDays: warningDays,
      displayState: that.data.displayState,
      displayType: that.data.displayType
    };
    app.api.getDisplayList(10, 1, param, that.getDisplayList)
    that.setData({
      warningDays: warningDays,
      currentList: [],
      pageCurrent: 1
    })
  },
  // 点位类型
  pointType: function () {
    this.setData({
      isShowCho: !this.data.isShowCho
    })
  },
  // 点位类型选择
  choiceType: function (e) {
    var that = this;
    var displayType = e.currentTarget.dataset.id;
    var idx = e.currentTarget.dataset.idx;
    if (that.data.isRent) {
      // 已出租
      var param = {
        isAdmin: 0,
        displayState: 1,
        displayType: displayType
      }
      app.api.getDisplayList(10, 1, param, that.getDisplayList)
      that.setData({
        displayType: displayType,
        currentCho: that.data.pointTypeList[idx].name,
        currentList: [],
        pageCurrent: '1'
      })
    } else {
      // 可租
      var param = {
        isAdmin: 0,
        displayState: 0,
        warningDays: that.data.warningDays,
        displayType: displayType
      }
      app.api.getDisplayList(10, 1, param, that.getDisplayList)
      that.setData({
        displayType: displayType,
        currentCho: that.data.pointTypeList[idx].name,
        currentList: [],
        pageCurrent: '1'
      })
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("上拉加载啊！")
    var that = this;
    if (that.data.isSearch) {
      var param = {
        isAdmin: 1,
        position: that.data.searchKey
      }
      app.api.getDisplayList(10, that.data.pageCurrent, param, that.getDisplayList);
    } else {
      if (that.data.displayState == 0) {
        var param = {
          isAdmin: '0',
          displayType: that.data.displayType,
          displayState: that.data.displayState,
          warningDays: that.data.warningDays
        }
        app.api.getDisplayList(10, that.data.pageCurrent, param, that.getDisplayList)
      } else {
        var param = {
          isAdmin: '0',
          displayType: that.data.displayType,
          displayState: that.data.displayState,
        }
        app.api.getDisplayList(10, that.data.pageCurrent, param, that.getDisplayList)
      }
    }

    // that.setData({
    //   displayType:that.data.displayType,
    //   displayState:that.data.displayState,
    //   warningDays:that.data.warningDays,
    // })

    console.log(param)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})