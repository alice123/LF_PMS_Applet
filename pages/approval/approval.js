var app = getApp();
var util = require('../../utils/util.js');
Page({
  data: {
    tabList:[
      {
        id:"0",
        title:'我发起',
      },
      {
        id:"1",
        title:'未审批',
      },
      {
        id:"2",
        title:'已通过',
      },
      {
        id:"3",
        title:'已驳回',
      }
    ],
    currentTabs: '0',
    currentTotalPage: 0,
    pageCurrent:1,
    current: '0',
    nowheight: 0,
    processRejectList:[],//已拒绝
    processPassList:[],//已通过
    processingList:[],//未审批
    myProcessList:[],//我发起
  },
  onLoad: function (options) {
    var that = this;
    that.init();
  },
  init:function(){
    var that = this;
    app.api.getMyProcess(10,1,that.getList);
  },
  choice:function(e){
    var that = this;
    var currentTabs = e.currentTarget.dataset.id;
    that.setData({
      currentTabs:currentTabs,
      pageCurrent:1
    })
    switch(currentTabs) {
      case '0':
        console.log("我发起")
        that.data.myProcessList = [];
        app.api.getMyProcess(10,1,that.getList);
        break;
      case '1':
        console.log("未审批")
        that.data.processingList = [];
        app.api.getProcessing(10,1,that.getList);
        break;
      case '2':
        console.log("已通过")
        that.data.processPassList = [];
        app.api.getProcessPass(10,1,that.getList);
        break;
          
      default:
        console.log("已驳回")
        that.data.processRejectList = [];
        app.api.getProcessReject(10,1,that.getList);
    }
  },
  getList:function(res){
    var that = this;
    if(res.code==200){
      var totalPage = Math.ceil(res.data.total / 10);
      if (that.data.pageCurrent <= totalPage){
        that.data.pageCurrent += 1;
        var newDataList = [];
        var serverDataList = res.data.records;
        for (var idx in serverDataList) {
          var serverData = serverDataList[idx];
          var tmp = {
            applyUserName: serverData.applyUserName,
            createBy: serverData.createBy,
            createDept: serverData.createDept,
            createTime: serverData.createTime?util.formatShortTime(serverData.createTime):'',
            createUserName: serverData.createUserName,
            doneTime: serverData.doneTime,
            formId: serverData.formId,
            id: serverData.id,
            instanceId: serverData.instanceId,
            processDefinitionId: serverData.processDefinitionId,
            reason: serverData.reason,
            sendType: serverData.sendType,
            sendUserIds: serverData.sendUserIds,
            statusInt: serverData.statusInt?serverData.statusInt:'1',
            superiorLeaders: serverData.superiorLeaders,
            taskId: serverData.taskId,
            taskName: serverData.taskName,
            title: serverData.title?serverData.title:serverData.itemName,
            updateBy: serverData.updateBy,
            updateTime: serverData.updateTime,
            reject:serverData.reject
          }
          newDataList.push(tmp)
        }
        if (that.data.currentTabs == '0') {
          that.setData({
            myProcessList: that.data.myProcessList.concat(newDataList)
          })
        } else if (that.data.currentTabs == '1') {
          that.setData({
            processingList: that.data.processingList.concat(newDataList)
          })
        } else if (that.data.currentTabs == '2') {
          that.setData({
            processPassList: that.data.processPassList.concat(newDataList)
          })
        }else if (that.data.currentTabs == '3') {
          that.setData({
            processRejectList: that.data.processRejectList.concat(newDataList)
          })
        }
      }
      wx.stopPullDownRefresh();
    }
   
  },
  // 查看详情
  go_detail:function(e){
    var that = this;
    var formId = e.currentTarget.dataset.formid;
    var idx = that.data.currentTabs;
    var taskId = e.currentTarget.dataset.taskid;
    var id = e.currentTarget.dataset.id;
    var reject = e.currentTarget.dataset.reject;
    if(idx=='1'){
      wx.navigateTo({
        url: '/pages/approval_detail/approval_detail?formId='+formId+'&currentTabs='+idx+'&taskId='+taskId+'&id='+id+"&reject="+reject,
      }) 
    }else{
      wx.navigateTo({
        url: '/pages/approval_detail/approval_detail?formId='+formId+'&currentTabs='+idx,
      }) 
    }
    
  },
  onReachBottom: function () {
    var that = this;
    switch (that.data.currentTabs) {
      case '0':
        app.api.getMyProcess(10,that.data.pageCurrent,that.getList);
        break;
      case '1':
        app.api.getProcessing(10,that.data.pageCurrent,that.getList);
        break;
      case '2':
        app.api.getProcessPass(10,that.data.pageCurrent,that.getList);
        break;
      case '3':
        app.api.getProcessReject(10,that.data.pageCurrent,that.getList);
    }
  }
})