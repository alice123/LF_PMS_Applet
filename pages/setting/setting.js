var app = getApp();
Page({
  data: {
    oldPwd:'',//旧密码
    newPwd:'',//新密码
    newPwdAgain:'',//再次输入密码
  },
  onLoad: function (options) {

  },
  oldpwd:function(e){
    console.log(e.detail.value)
    this.setData({
      oldPwd:e.detail.value
    })
  },
  newpwd:function(e){
    console.log(e.detail.value)
    this.setData({
      newPwd:e.detail.value
    })
  },
  newpwdagain:function(e){
    console.log(e.detail.value)
    this.setData({
      newPwdAgain:e.detail.value
    })
    
  },
  edit:function(){
    var that = this;
    if(!that.data.oldPwd){
      wx.showToast({
        title: '请输入原密码',
        icon:'none'
      })
      return false;
    }
    if(!that.data.newPwd){
      wx.showToast({
        title: '请输入新密码',
        icon:'none'
      })
      return false;
    }
    if(!that.data.newPwdAgain){
      wx.showToast({
        title: '请再次输入新密码',
        icon:'none'
      })
      return false;
    }
    if(that.data.newPwdAgain!=that.data.newPwd&&that.data.newPwd&&that.data.newPwdAgain){
      wx.showToast({
        title: '两次密码不同',
        icon:'none'
      })
      return false;
    }

    // 修改密码接口
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})