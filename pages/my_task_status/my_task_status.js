var app = getApp();
Page({
  data: {
    todayFinished:'0',
    todayTotal:'0',
    overTimeFinished:'0',
    overTimeTotal:'0',
    expectationFinished:'0',
    expectationTotal:'0'
  },
  onLoad: function (options) {
    var that = this;
    app.api.countTask(that.countTask)
  },
  countTask:function(res){
    var that = this;
    console.log(res)
    var list = res.data;
    
    that.setData({
      todayFinished:list.today.finished?list.today.finished:'0',
      todayTotal:list.today.total?list.today.total:'0',
      overTimeFinished:list.overtime.finished?list.overtime.finished:'0',
      overTimeTotal:list.overtime.total?list.overtime.total:'0',
      expectationFinished:list.expectation.finished?list.expectation.finished:'0',
      expectationTotal:list.expectation.total?list.expectation.total:'0'
    })
  },
  choState:function(e){
    var that = this;
    var id = e.currentTarget.dataset.id;
    console.log(id)
    switch(id) {
      case '1':
         wx.navigateTo({
           url: '/pages/my_task/my_task?name=今日任务&queryType=1',
         })
         break;
      case '2':
        wx.navigateTo({
          url: '/pages/my_task/my_task?name=逾期任务&queryType=2',
        })
         break;
      default:
        wx.navigateTo({
          url: '/pages/my_task/my_task?name=预期任务&queryType=3',
        })
      } 
  }
})