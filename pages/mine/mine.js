var app = getApp();
Page({
  data: {
    username: '',
    station: '',
    phone: '',
    avatar: '',
    key: '',
    userid: '',
    staffid: '',
    token: '',
    current: 'mine',
    unLogin: false,
    ischo: false,
    istel: false,
    ispic: false,
    isUnLogin: false,
    isdy: false,
    isMaskHidden: false,
    navindex:'1',
    navrole:'0'
  },
  onLoad: function (options) {
    
  },
  onShow:function(){
    var that = this;
    var token = wx.getStorageSync('token');
    that.init();
    if (!token) {
      that.setData({
        unLogin: true
      })
      return false;
    } else {

      app.api.userDetail(that.userDetail);
    }
  },
  init: function () {
    var that = this;
    that.setData({
      istel: false,
      ispic: false,
      isdy: false
    })
    wx.login({
      success(res) {
        if (res.code) {
          var data = {
            code: res.code
          };
          app.api.authorize(data).then(function (res) {
            console.log("查看token")
            console.log(res)
            if (res.code == 200) {
              // 已授权，用token获取个人信息
              wx.setStorageSync('token', res.data.access_token);
              that.setData({
                unLogin: false
              })
              app.api.userDetail(that.userDetail);
            } else {
              //未授权
              that.setData({
                unLogin: true
              })
              wx.clearStorage('token');
            }
          })
        } else {
          wx.clearStorage('token');
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
  },
  userDetail: function (res) {
    var that = this;
    if (res.code == 200) {
      that.setData({
        username: res.data.user.username,
        station: res.data.user.staffName,
        avatar: res.data.user.avatar
      })
      console.log(res.data,res.data.isManager,'data')
      if(res.data.isManager==1){
      
        that.setData({
          navindex:'2',
          navrole:'1'
        })
      }else{
      
        that.setData({
          navindex:'1',
          navrole:'0'
        })
      }
      wx.setStorageSync('user', res.data);
      console.log(res)
    } else {
      wx.showToast({
        title: res.msg,
        icon: 'none'
      })
    }
  },
  getTel: function () {
    var that = this;
    that.setData({
      istel: true,
      isMaskHidden: true
    })
  },
  // 微信授权手机号码登录
  getPhoneNumber: function (e) {
    console.log("解密前…………………………………………………………………………………………")
    console.log(e.detail)
    var that = this;
    that.setData({
      ischo: true
    })

    if (e.detail.encryptedData == undefined) {
      app.globalData.token = '';
      that.setData({
        ischo: false
      })
    } else {
      app.api.getPhone({
        code: app.globalData.code,
        iv: e.detail.iv,
        encryptedData: e.detail.encryptedData
      }, (res) => {
        console.log('解密后的电话号码****************************', res.data.phoneNumber)
        app.globalData.mobile = res.data.phoneNumber;
        app.globalData.iv = e.detail.iv;
        wx.setStorageSync('phoneNumber', res.data.phoneNumber);

        that.setData({
          istel: false,
          ispic: true,
          isdy: false
        })
      })
    }
  },
  getUserInfo: function (e) {
    console.log(e.detail)
    var that = this;
    api.register({
      encryptedData: e.detail.encryptedData,
      iv: e.detail.iv
    }, that.register);
  },
  register: function (res) {
    var that = this;
    if (res.code == 200) {
      that.init();
      that.setData({
        isdy: true,
        ispic: false,
        istel: false,
        isMaskHidden: false
      })

    } else {
      // wx.showToast({
      //   title: res.msg,
      //   icon:'none'
      // })
      console.log(res.msg)
      that.setData({
        ispic: false,
        isUnLogin: true,
        isdy: false
      })
    }
  },
  cancelUnLogin: function () {
    var that = this;
    that.setData({
      istel: false,
      ischo: false,
      isdy: false,
      isUnLogin: false
    })
  },
  cancelTel: function () {
    var that = this;
    that.setData({
      istel: false,
      isdy: false,
      ischo: false
    })
  },
  cancelPic: function () {
    var that = this;
    that.setData({
      ispic: false,
      isdy: false,
      ischo: false
    })
  },
  cancelDY: function () {
    var that = this;
    that.setData({
      ispic: false,
      isdy: false,
      ischo: false
    })
  },
  // 消息订阅
  getFormId: function (e) {
    var that = this;
    that.setData({
      ispic: false,
      isdy: false,
      istel: false
    })
    wx.requestSubscribeMessage({
      tmplIds: ['ExllcLCogTTuoRiXGtbBTcd8wsBoFhbiS5_o3sgLmME'],
      success(res) {
        if (res.ExllcLCogTTuoRiXGtbBTcd8wsBoFhbiS5_o3sgLmME == 'accept') {
          var param = {
            type: 1
          };
          app.api.weixinTemplate(param, function (res) {
            console.log(res)
            if (res.code == 200) {
              var arr = [];
              var list = res.data;
              for (var i in list) {
                arr.push(list[i].id)
              }
              app.api.weixinTemplateSubscribe(arr, function (res) {
                console.log(res)
                if (res.code == 200) {
                  wx.showToast({
                    title: '订阅成功！',
                    icon: 'none'
                  })

                }

              })
              console.log(arr)
              //保存订阅消息的模板记录
            }
          })
        }
        // console.log(res)
      }
    })
  }
})