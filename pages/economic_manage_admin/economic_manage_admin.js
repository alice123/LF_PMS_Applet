var app = getApp();
Page({
  data: {
    searchKey:'',
    displayState:0,
    displayType:'',//点位类型
    displayRank:'',//点位级别
    currentCho:'点位类型',//显示点位的名字
    pointTypeList:[],
    pointLevelList:[],
    rentList:[
      {label:'可租赁'},
      {label:'已出租'},
      {label:'全部'}
    ],
    currentList:[],
    isShowCho:false,
    isLevelCho:false,
    currentLevel:'点位级别',
    searchList:[],
    isSearch:false,
    pageCurrent: 1,
  },
  onLoad: function (options) {
    var that = this;
    // 获取【点位类型】
    app.api.getDisplayType(function(res){
      that.setData({
        pointTypeList:res.data
      })
    })
    // 获取【点位级别】
    app.api.getDisplayRank(function(res){
      that.setData({
        pointLevelList:res.data
      })
    })
    that.initList();
  },
  initList:function(){
    var that = this;
    var param = {
      isAdmin:1,
      displayState:0
    };
    app.api.getDisplayList(10,1,param,that.getDisplayList);
  },
  getDisplayList:function(res){
    var that = this;
    console.log("走到这步了…………………………"+that.data.pageCurrent)
    var totalPage = Math.ceil(res.data.total / 10);
    if (that.data.pageCurrent <= totalPage){
      that.data.pageCurrent += 1;
      var newDataList = [];
      var serverDataList = res.data.records;
      for (var idx in serverDataList) {
        var obj = serverDataList[idx];
        var tmp = {
          area:obj.area?obj.area:'0',
          deptId:obj.deptId,
          deptName:obj.deptName,
          displayNo:obj.displayNo,
          displayPicUrl:obj.displayPicUrl,
          displayRank:obj.displayRank,
          displaySize:obj.displaySize,
          displayState:obj.displayState,
          displaySubType:obj.displaySubType,
          displaySubTypeName:obj.displaySubTypeName,
          displayType:obj.displayType,
          endTime:obj.endTime,
          id:obj.id,
          mobile:obj.mobile,
          position:obj.position,
          staffId:obj.staffId,
          staffName:obj.staffNam?obj.staffName:'',
          startTime:obj.startTime,
          terminateTime:obj.terminateTime,
          warningDays:obj.warningDays,
        };
        newDataList.push(tmp)
      }
      console.log(that.data.isSearch)
      console.log(newDataList)
      
      if(that.data.isSearch){
        that.setData({
          pageCurrent:that.data.pageCurrent,
          searchList:that.data.searchList.concat(newDataList)
        })
      }else{
        that.setData({
          pageCurrent:that.data.pageCurrent,
          currentList:that.data.currentList.concat(newDataList)
        })
      }
      
    }
    wx.stopPullDownRefresh();
  },
  //预览图片
  perViewPic:function(e){
    var that = this;
    var src = e.currentTarget.dataset.url;//获取data-src
    var imgList = [e.currentTarget.dataset.url];//获取data-list
    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    })
  },
  searchInput:function(e){
    console.log(e.detail.value)
    var that = this;
    that.setData({
      searchKey:e.detail.value
    })
  },
  gosearch:function(){
    console.log("去搜索")
    var that = this;
   
    if(!that.data.searchKey){
      console.log(11)
      that.setData({
        isSearch:false,
        searchList:[],
        pageCurrent:1
      })
    }else{
      console.log(22)
      that.setData({
        isSearch:true,
        pageCurrent:1
      })
      console.log(that.data.isSearch)
      var param ={
        isAdmin:1,
        position:that.data.searchKey
      }
      app.api.getDisplayList(10,1,param,that.getDisplayList);
      
    }
  },
  //【可租赁】、【已出租】、【全部】
  choRent:function(e){
    var that = this;
    var displayState = e.currentTarget.dataset.idx;
    if(displayState==0||displayState==1){
      var param = {
        isAdmin:1,
        displayState:displayState,
        displayType:that.data.displayType,
        displayRank:that.data.displayRank
      };
      app.api.getDisplayList(10,1,param,that.getDisplayList);
    }else{
      var param = {
        isAdmin:1,
        displayType:that.data.displayType,
        displayRank:that.data.displayRank
      };
      app.api.getDisplayList(10,1,param,that.getDisplayList);
    }
    
    that.setData({
      displayState:displayState,
      pageCurrent:1,
      currentList:[]
    })
  },
  //选择点位类型
  pointType:function(){
    this.setData({
      isShowCho:!this.data.isShowCho,
      isLevelCho:false
    })
  },
  choiceType:function(e){
    var that = this;
    var displayType = e.currentTarget.dataset.id;
    var choiceIdx = e.currentTarget.dataset.idx;
    if(that.data.displayState==2){
      var param = {
        isAdmin:1,
        displayType:displayType,
        displayRank:that.data.displayRank
      };
    }else{
      var param = {
        isAdmin:1,
        displayType:displayType,
        displayRank:that.data.displayRank,
        displayState:that.data.displayState
      };
    }
    app.api.getDisplayList(10,1,param,that.getDisplayList);
    that.setData({
      displayType:displayType,
      currentCho:that.data.pointTypeList[choiceIdx].name,
      isShowCho:false,
      isLevelCho:false,
      pageCurrent:1,
      currentList:[]
    })
  },
  // 选择点位级别
  pointLevel:function(){
    this.setData({
      isLevelCho:!this.data.isLevelCho,
      isShowCho:false
    })
  },
  choiceLevel:function(e){
    var that = this;
    var displayRank = e.currentTarget.dataset.id;
    var choiceIdx = e.currentTarget.dataset.idx;
    if(that.data.displayState==2){
      var param = {
        isAdmin:1,
        displayType:that.data.displayType,
        displayRank:displayRank
      };
    }else{
      var param = {
        isAdmin:1,
        displayType:that.data.displayType,
        displayRank:displayRank,
        displayState:that.data.displayState
      };
    }
    app.api.getDisplayList(10,1,param,that.getDisplayList);
    that.setData({
      displayRank:displayRank,
      currentLevel:that.data.pointLevelList[choiceIdx].name,
      isShowCho:false,
      isLevelCho:false,
      pageCurrent:1,
      currentList:[]
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    if(that.data.isSearch){
      var param ={
        isAdmin:1,
        position:that.data.searchKey
      }
      app.api.getDisplayList(10,that.data.pageCurrent,param,that.getDisplayList);
    }else{
      if(that.data.displayState==2){
        var param  = {
          isAdmin:1,
          displayType:that.data.displayType,
          displayRank:that.data.displayRank,
        }
        app.api.getDisplayList(10,that.data.pageCurrent,param,that.getDisplayList);
      }else{
        var param  = {
          isAdmin:1,
          displayState:that.data.displayState,
          displayType:that.data.displayType,
          displayRank:that.data.displayRank,
        }
        app.api.getDisplayList(10,that.data.pageCurrent,param,that.getDisplayList);
      }
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})