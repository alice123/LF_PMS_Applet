module.exports = {
    // 请求域名 格式： https://您的域名
    HTTP_REQUEST_URL: 'https://pms.lfsmart.net/mini/',//华润演示服
    // HTTP_REQUEST_URL: 'http://106.52.196.11:7071/',
    // HTTP_REQUEST_URL: 'http://106.52.54.73:7071/',
    // HTTP_REQUEST_URL: 'https://test.ultraai.net/',//测试服务器线上的
    // HTTP_REQUEST_URL:'https://sfpmsuat.crland.com.cn/mini/',//华润服务器
    // HTTP_REQUEST_URL: 'http://106.52.54.73:7071/',//测试服务器
    // HTTP_REQUEST_URL: 'http://192.168.1.191:8081/' //德哥
    // HTTP_REQUEST_URL: 'http://192.168.1.173:7071/' //强哥【测试服】
    // HTTP_REQUEST_URL: 'http://192.168.1.173:8081/' //强哥【开发服】
    // HTTP_REQUEST_URL: 'http://192.168.1.192:7071/',//浩哥
    // HTTP_REQUEST_URL: 'http://192.168.1.134:8081/' //婷
};